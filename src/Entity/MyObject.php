<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\MyObjectRepository")
 */
class MyObject
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    /**
     * @Assert\NotBlank (message = "Le numéro ne peut pas être vide")
     */
    /**
     * @Assert\Range(
     *      min = 10,
     *      max = 50,
     *      minMessage = "Le numéro doit être supérieur à {{ limit }}.",
     *      maxMessage = "Le numéro doit être inférieur à {{ limit }}."
     * )
     */
    private $number;

    /**
     * @ORM\Column(type="string", length=255, nullable=false)
     */
    /**
     * @Assert\NotBlank (message = "Le nom ne peut pas être vide")

     */

      /**
     * @Assert\Regex(
     *     pattern     = "/^[a-z]+$/i",
     *     htmlPattern = "^[a-zA-Z0-9]+$",
     *     message = "Pas de caractères spéciaux svp"

     * )
     */
    private $name;

    /**
     * @Assert\Choice({"clothes", "game", "sauerkraut"}, message = "Les 3 choix possibles sont clothes, game ou sauerkraut")
     */

    private $category;

    public function __toString()
    {
        if ($this->getName()) {
            return $this->getName();
        } else {
            return 'no name';
        }
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNumber(): ?int
    {
        return $this->number;
    }

    public function setNumber(?int $number): self
    {
        $this->number = $number;
        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(?string $name): self
    {
        $this->name = $name;

        return $this;
    }
    public function getCategory(): ?string
    {
        return $this->category;
    }

    public function setCategory(?string $category): self
    {
        $this->category = $category;
        return $this;
    }
}
