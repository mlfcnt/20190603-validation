Validation exercise
===================


# What does this code ?

# Configure the database, what happends if you validate the form on localhost/my-object/add with empty fields ? Why ?


name can't be null 

# Add validation of the field "name" to avoid the problem using validation :
https://symfony.com/doc/current/validation.html

# Add validation on number : it must be between 10 and 50

# Add a string field "category" which must be either "clothes", "game" or "sauerkraut"

# Add validation on name : it must contain only letters and numbers
